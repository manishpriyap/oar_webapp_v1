package com.jspider.springweb.controller;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jspider.springweb.dto.RegisterDto;

@Component
@RequestMapping("/")
public class RegisterController {

	public RegisterController() {
		System.out.println(this.getClass().getSimpleName() + " object created");
	}
	@RequestMapping(value = "/saveRegisterDetails")
	public ModelAndView saveRegisterDetails(RegisterDto registerDto) {
		System.out.println(registerDto);
		return new ModelAndView("home.jsp");
	}
	
}
