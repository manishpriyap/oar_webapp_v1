package com.jspider.springweb.dto;

import java.io.Serializable;
import java.util.Date;

public class RegisterDto implements Serializable{

	private String name;
	
	private String email;
	
	private String mobileNumber;
	
	private String country;
	
	private String password;
	
	private String dateOfBirth;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@Override
	public String toString() {
		return "RegisterDto [name=" + name + ", email=" + email + ", mobileNumber=" + mobileNumber + ", country="
				+ country + ", password=" + password + ", dateOfBirth=" + dateOfBirth + "]";
	}
	
	
	
}
